import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebviewMenu extends StatefulWidget {
  WebviewMenu({Key? key}) : super(key: key);

  @override
  _WebviewMenuState createState() => _WebviewMenuState();
}

class _WebviewMenuState extends State<WebviewMenu> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: WebviewScaffold(
        url: 'https://www.instagram.com/hilmyfdhl/',
        hidden: true,
        appBar: AppBar(
          backgroundColor: Colors.amber[200],
          title: Text('Instagram User'),
        ),
      ),
    );
  }
}
