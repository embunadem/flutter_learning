import 'package:flutter/material.dart';

class IconbadgeMenu extends StatelessWidget {
  const IconbadgeMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Icon badge menu'),
        ),
        body: Center(
          child: Row(
            //biar tengah
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildIconBadge(Icons.home, '18', Colors.red),
              _buildIconBadge(Icons.notifications, '20', Colors.blue),
              _buildIconBadge(Icons.message, '120', Colors.green),
              _buildIconBadge(Icons.email, '99', Colors.orange),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buildIconBadge(
  IconData icon,
  String badgeText,
  Color badgeColor,
) {
  return Stack(
    children: [
      Icon(
        icon,
        size: 50,
      ),
      Positioned(
        top: 2,
        right: 4,
        child: Container(
          padding: EdgeInsets.all(1),
          decoration: BoxDecoration(
            color: badgeColor,
            shape: BoxShape.circle,
          ),
          constraints: BoxConstraints(
            maxHeight: 18,
            minHeight: 18,
          ),
          child: Center(
            child: Text(
              badgeText,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 10,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      )
    ],
  );
}
