import 'package:flutter/material.dart';

class ListviewMenu extends StatelessWidget {
  const ListviewMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('ListView Builder Menu'),
        ),
        body: PageList(),
      ),
    );
  }
}

class PageList extends StatefulWidget {
  PageList({Key? key}) : super(key: key);

  @override
  _PageListState createState() => _PageListState();
}

class _PageListState extends State<PageList> {
  List<String> fruits = [
    'Mangga',
    'Pepaya',
    'Anggur',
    'Pisang',
    'Jeruk',
    'Semangka',
    'Apel',
    'Rambutan',
    'Buah Naga',
    'Durian',
    'Kelengkeng',
    'Duku',
    'Jambu',
    'Jambu Monyet',
    'Jambu Biji',
    'Jambu Air',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.brown[400],
      child: ListView.builder(
        itemCount: fruits.length,
        itemBuilder: (BuildContext context, int index) {
          final number = index + 1;
          final buahan = fruits[index].toString();
          return Card(
            child: ListTile(
              leading: Text(number.toString()),
              title: Text(buahan),
              trailing: Icon(Icons.check),
            ),
          );
        },
      ),
    );
  }
}
