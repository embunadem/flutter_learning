import 'package:belajar_flutter3/page3.dart';
import 'package:flutter/material.dart';
import './page1.dart';

class MenuUtama extends StatefulWidget {
  MenuUtama({Key? key}) : super(key: key);

  @override
  _MenuUtamaState createState() => _MenuUtamaState();
}

class _MenuUtamaState extends State<MenuUtama> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Navigation Page'),
        backgroundColor: Colors.brown[400],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                //go to page 1
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return Page1();
                }));
              },
              child: Text('Go to Page 1'),
              color: Colors.blue,
            ),
            RaisedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return Page3();
                }));
              },
              child: Text('Go to Page 3'),
              color: Colors.green,
            )
          ],
        ),
      ),
    );
  }
}
