import 'package:flutter/material.dart';

class ChipwidgetMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Chip widget menu'),
        ),
        body: Center(
          child: Column(
            children: [
              Chip(
                label: Text('Flutter'),
                avatar: CircleAvatar(
                  child: Icon(Icons.camera),
                ),
              ),
              Chip(
                label: Text('Flutter'),
                avatar: CircleAvatar(
                  backgroundColor: Colors.red,
                  child: Icon(Icons.share),
                ),
              ),
              Chip(
                label: Text('Internio'),
                avatar: CircleAvatar(
                  backgroundColor: Colors.green,
                  child: Text('In'),
                ),
              ),
              InputChip(
                label: Text('internio@gmail.com'),
                onPressed: () {},
                avatar: CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://assets.turbologo.com/blog/en/2019/10/19084944/youtube-logo-illustration.jpg'),
                ),
                onDeleted: () {
                  //mirip console
                  debugPrint('delete chip disini');
                },
                deleteIcon: Icon(Icons.delete),
              )
            ],
          ),
        ),
      ),
    );
  }
}
