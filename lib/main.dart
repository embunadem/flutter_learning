import 'package:belajar_flutter3/cardwidget-menu.dart';
import 'package:belajar_flutter3/chipwidget-menu.dart';
import 'package:belajar_flutter3/clipoval-menu.dart';
import 'package:belajar_flutter3/convexbottom-menu.dart';
import 'package:belajar_flutter3/dialog-menu.dart';
import 'package:belajar_flutter3/icon-badge.dart';
import 'package:belajar_flutter3/listview-menu.dart';
import 'package:belajar_flutter3/menu.dart';
import 'package:belajar_flutter3/nav_drawer.dart';
import 'package:belajar_flutter3/pageselector-menu.dart';
import 'package:belajar_flutter3/pageview-menu.dart';
import 'package:belajar_flutter3/password_hide.dart';
import 'package:belajar_flutter3/pdfviewer-menu.dart';
import 'package:belajar_flutter3/sliver_menu.dart';
import 'package:belajar_flutter3/web_view.dart';
import 'package:belajar_flutter3/widget_test.dart';
import 'package:flutter/material.dart';
import './menu_utama.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: MenuUtama(),
      // home: WidgetTest(),
      // home: Menu(),
      // home: NavDrawer(),
      // home: SliverMenu(),
      // home: PasswordHideMenu(),
      // home: WebviewMenu(),
      // home: ListviewMenu(),
      // home: IconbadgeMenu(),
      // home: ConvexbottomMenu(),
      // home: PdfviewerMenu(),
      // home: ClipovalMenu(),
      // home: PageviewMenu(),
      // home: CardwidgetMenu(),
      // home: ChipwidgetMenu(),
      // home: DialogMenu(),
      home: PageselectorMenu(),
    );
  }
}

// class _MyAppState extends State<MyApp> {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'First App',
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         backgroundColor: Colors.white,
//         appBar: AppBar(
//           title: Center(child: Text('Aplikasi Pertama')),
//           //bisa pake refactor buat bungkus title ke widget
//           backgroundColor: Colors.brown[400],
//           elevation: 0.0, //shadow appbar ke body
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//               icon: Icon(Icons.attach_money),
//               onPressed: () {},
//             ),
//             IconButton(
//               icon: Icon(Icons.logout),
//               onPressed: () {},
//             ),
//             PopupMenuButton(itemBuilder: (BuildContext context) {
//               return [
//                 PopupMenuItem(child: Text('Menu 1')),
//                 PopupMenuItem(child: Text('Menu 2')),
//                 PopupMenuItem(child: Text('Menu 3')),
//                 PopupMenuItem(child: Text('Menu 4')),
//               ];
//             })
//           ],
//         ),
//         // body: Center(
//         //   child: Text('Hello World'),
//         // ),
//         //===== TEXT WIDGET =====
//         // body: ListView(
//         //   children: <Widget>[
//         //     Text('Tulisan disini 1'),
//         //     Text(
//         //       'Tulisan disini 2',
//         //       style: TextStyle(fontStyle: FontStyle.italic, fontSize: 25),
//         //     ),
//         //     Text(
//         //       'Tulisan disini 3',
//         //       style: TextStyle(
//         //           fontStyle: FontStyle.normal,
//         //           fontSize: 30,
//         //           color: Colors.blueAccent,
//         //           decoration: TextDecoration.underline,
//         //           fontWeight: FontWeight.bold),
//         //     ),
//         //   ],
//         // ),
//         //===== ICON WIDGET =====
//         // body: Center(
//         //   child: Column(
//         //     mainAxisAlignment: MainAxisAlignment.spaceAround,
//         //     children: <Widget>[
//         //       Icon(
//         //         Icons.shopping_bag,
//         //         size: 50,
//         //         color: Colors.blue,
//         //       ),
//         //       Icon(
//         //         Icons.map,
//         //         size: 50,
//         //         color: Colors.yellow,
//         //       ),
//         //       Icon(
//         //         Icons.favorite,
//         //         size: 80,
//         //         color: Colors.pink,
//         //       ),
//         //       Icon(
//         //         Icons.umbrella,
//         //         size: 50,
//         //         color: Colors.green,
//         //       ),
//         //     ],
//         //   ),
//         // ),
//         //===== TEXT FIELD WIDGET =====
//         // body: Container(
//         //   margin: EdgeInsets.all(20),
//         //   child: ListView(
//         //     children: <Widget>[
//         //       TextField(),
//         //       SizedBox(
//         //         height: 15,
//         //       ),
//         //       TextField(
//         //         decoration: InputDecoration(
//         //           border: OutlineInputBorder(),
//         //           icon: Icon(Icons.people),
//         //           hintText: 'Input username here',
//         //         ),
//         //         maxLength: 10,
//         //         // controller: TextEditingController(text: 'Input name'),
//         //       ),
//         //       SizedBox(
//         //         height: 15,
//         //       ),
//         //       TextField(
//         //         obscureText: true,
//         //         decoration: InputDecoration(
//         //           border: OutlineInputBorder(),
//         //           icon: Icon(Icons.people),
//         //           hintText: 'Input password here',
//         //         ),
//         //         // controller: TextEditingController(text: 'Input name'),
//         //       ),
//         //       SizedBox(
//         //         height: 15,
//         //       ),
//         //       TextField(
//         //         keyboardType: TextInputType.number,
//         //         decoration: InputDecoration(
//         //           border: OutlineInputBorder(),
//         //           icon: Icon(Icons.monetization_on),
//         //           prefixText: 'Rp. ',
//         //           prefixStyle: TextStyle(
//         //             color: Colors.orange,
//         //             fontSize: 20,
//         //           ),
//         //         ),
//         //         maxLength: 10,
//         //         // controller: TextEditingController(text: 'Input name'),
//         //       ),
//         //       SizedBox(
//         //         height: 15,
//         //       ),
//         //       TextField(
//         //         decoration: InputDecoration(
//         //           border: OutlineInputBorder(),
//         //           labelText: 'Waktu',
//         //           prefixStyle: TextStyle(
//         //             color: Colors.orange,
//         //             fontSize: 20,
//         //           ),
//         //           prefixIcon: Icon(Icons.access_alarm),
//         //         ),
//         //         maxLength: 10,
//         //         // controller: TextEditingController(text: 'Input name'),
//         //       ),
//         //       SizedBox(
//         //         height: 15,
//         //       ),
//         //       TextField(
//         //         maxLines: 5,
//         //         decoration: InputDecoration(
//         //           border: OutlineInputBorder(),
//         //           icon: Icon(Icons.description),
//         //         ),
//         //         maxLength: 250,
//         //         keyboardType: TextInputType.multiline,
//         //       ),
//         //     ],
//         //   ),
//         // ),
//         //===== IMAGE WIDGET =====
//         // body: Container(
//         //   margin: EdgeInsets.all(20),
//         //   child: ListView(
//         //     children: <Widget>[
//         //       Text('Image dari url'),
//         //       Image(
//         //         image: NetworkImage(
//         //             'https://cdn.dribbble.com/users/17559/screenshots/6664357/figma.png'),
//         //         width: 200,
//         //         height: 200,
//         //       ),
//         //       Text('Image dari assets'),
//         //       Image(
//         //         image: AssetImage('assets/InternioFinal.png'),
//         //         width: 200,
//         //         height: 200,
//         //       )
//         //     ],
//         //   ),
//         // ),
//         //===== BUTTON WIDGET =====
//         // body: Container(
//         //   margin: EdgeInsets.all(20),
//         //   child: ListView(
//         //     children: <Widget>[
//         //       Text('Material Button'),
//         //       MaterialButton(
//         //         child: Text(
//         //           'Button Pertama',
//         //           style: TextStyle(color: Colors.white),
//         //         ),
//         //         color: Colors.brown,
//         //         onPressed: () {},
//         //       ),
//         //       MaterialButton(
//         //         padding: EdgeInsets.symmetric(
//         //           vertical: 15,
//         //           horizontal: 20,
//         //         ),
//         //         shape: RoundedRectangleBorder(
//         //             borderRadius: BorderRadius.circular(10)),
//         //         child: Text(
//         //           'Button Kedua',
//         //           style: TextStyle(color: Colors.white),
//         //         ),
//         //         color: Colors.blue,
//         //         onPressed: () {},
//         //       ),
//         //       Text('Raize Button'),
//         //       RaisedButton(
//         //         padding: EdgeInsets.symmetric(
//         //           vertical: 30,
//         //           horizontal: 30,
//         //         ),
//         //         onPressed: () {},
//         //         color: Colors.red,
//         //         child: Text(
//         //           'Button ketiga',
//         //           style: TextStyle(color: Colors.white),
//         //         ),
//         //       ),
//         //       Text('Flat Button'),
//         //       FlatButton(
//         //         padding: EdgeInsets.symmetric(
//         //           vertical: 20,
//         //           horizontal: 20,
//         //         ),
//         //         onPressed: () {},
//         //         child: Text(
//         //           'Button keempat',
//         //           style: TextStyle(color: Colors.white),
//         //         ),
//         //         color: Colors.yellow,
//         //       ),
//         //       Text('Icon Button'),
//         //       IconButton(
//         //         onPressed: () {},
//         //         icon: Icon(Icons.save),
//         //         color: Colors.purple,
//         //         iconSize: 40,
//         //       ),
//         //       Text('Outline Button'),
//         //       OutlineButton(
//         //         onPressed: () {},
//         //         child: Text(
//         //           "Button kelima",
//         //         ),
//         //       ),
//         //     ],
//         //   ),
//         // ),
//         // ===== CONTAINTER WIDGET =====
//         // body: Container(
//         //   color: Colors.amber,
//         //   margin: EdgeInsets.all(20),
//         //   child: Container(
//         //     color: Colors.red,
//         //     margin: EdgeInsets.all(20),
//         //     child: Container(
//         //       // color: Colors.green,
//         //       decoration: BoxDecoration(
//         //         gradient: LinearGradient(
//         //           begin: Alignment.topLeft,
//         //           end: Alignment.bottomRight,
//         //           colors: <Color>[
//         //             Colors.yellow,
//         //             Colors.blue,
//         //           ],
//         //         ),
//         //         borderRadius: BorderRadius.circular(20),
//         //       ),
//         //       margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
//         //     ),
//         //   ),
//         // ),
//         // ===== ROW ======
//         // body: Container(
//         //   color: Colors.amber,
//         //   child: Row(
//         //     //main itu horizontal
//         //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         //     //cross itu vertikal
//         //     crossAxisAlignment: CrossAxisAlignment.start,
//         //     children: <Widget>[
//         //       Icon(
//         //         Icons.alarm,
//         //         size: 40,
//         //       ),
//         //       Icon(
//         //         Icons.alarm,
//         //         size: 80,
//         //       ),
//         //       Icon(
//         //         Icons.alarm,
//         //         size: 30,
//         //       ),
//         //     ],
//         //   ),
//         // ),
//         // ===== COLUMN ======
//         // body: Container(
//         //   color: Colors.green[200],
//         //   child: Column(
//         //     mainAxisAlignment: MainAxisAlignment.spaceAround,
//         //     crossAxisAlignment: CrossAxisAlignment.center,
//         //     children: <Widget>[
//         //       Icon(
//         //         Icons.home,
//         //         size: 40,
//         //       ),
//         //       Icon(
//         //         Icons.home,
//         //         size: 60,
//         //       ),
//         //       Icon(
//         //         Icons.home,
//         //         size: 80,
//         //       ),
//         //       Icon(
//         //         Icons.home,
//         //         size: 100,
//         //       ),
//         //       Text('Bagian dari Column')
//         //     ],
//         //   ),
//         // ),
//       ),
//     );
//   }
// }
