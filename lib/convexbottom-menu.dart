import 'package:belajar_flutter3/convex-home.dart';
import 'package:belajar_flutter3/convex-info.dart';
import 'package:belajar_flutter3/convex-price.dart';
import 'package:belajar_flutter3/convex-store.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';

//DOWNLOAD PACKAGE DLU DI PUB.DEV
class ConvexbottomMenu extends StatefulWidget {
  ConvexbottomMenu({Key? key}) : super(key: key);

  @override
  _ConvexbottomMenuState createState() => _ConvexbottomMenuState();
}

class _ConvexbottomMenuState extends State<ConvexbottomMenu> {
  int selectedPage = 1;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Convex bottom bar menu'),
          backgroundColor: Colors.brown[400],
        ),
        body: [
          ConvexHomeMenu(),
          ConvexStoreMenu(),
          ConvexPriceMenu(),
          ConvexInfoMenu(),
        ][selectedPage],
        bottomNavigationBar: ConvexAppBar(
          backgroundColor: Colors.brown[400],
          style: TabStyle.flip,
          items: [
            TabItem(
              icon: Icons.home,
              title: 'Home',
            ),
            TabItem(
              icon: Icons.store,
              title: 'Store',
            ),
            TabItem(
              icon: Icons.monetization_on,
              title: 'Price',
            ),
            TabItem(
              icon: Icons.info,
              title: 'Info',
            ),
          ],
          //harus sama selected page
          initialActiveIndex: 1,
          onTap: (int i) {
            setState(() {
              selectedPage = i;
            });
          },
        ),
      ),
    );
  }
}
