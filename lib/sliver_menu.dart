import 'package:flutter/material.dart';

class SliverMenu extends StatefulWidget {
  SliverMenu({Key? key}) : super(key: key);

  @override
  _SliverMenuState createState() => _SliverMenuState();
}

//===== SLIVER APP BAR =====
class _SliverMenuState extends State<SliverMenu> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              pinned: true,
              expandedHeight: 180,
              backgroundColor: Colors.brown[400],
              flexibleSpace: FlexibleSpaceBar(
                title: Text('Sliver AppBar'),
                background: Image.asset(
                  'assets/stormtrooper.jpg',
                  fit: BoxFit.fill,
                ),
              ),
            ),
            SliverFillRemaining(
              child: Center(
                child: Text('Body Disini'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
