import 'package:flutter/material.dart';

class CardwidgetMenu extends StatelessWidget {
  const CardwidgetMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Card Widget'),
        ),
        body: ListView(
          children: [
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 1'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: ClipOval(
                  child: Image.network(
                    'https://infobinatang.com/wp-content/uploads/2019/08/cropped-gambar-British-Shorthair.jpg',
                    fit: BoxFit.fill,
                    width: 50,
                    height: 80,
                  ),
                ),
                title: Text('Mahasiswa 3'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              shadowColor: Colors.yellow,
              elevation: 10,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 4'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 5'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 6'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
            Card(
              elevation: 7,
              child: ListTile(
                leading: Icon(
                  Icons.person,
                  size: 50,
                ),
                title: Text('Mahasiswa 2'),
                subtitle: Text(
                    't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
