import 'package:flutter/material.dart';

class MenuTab1 extends StatefulWidget {
  MenuTab1({Key? key}) : super(key: key);

  @override
  _MenuTab1State createState() => _MenuTab1State();
}

class _MenuTab1State extends State<MenuTab1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Page Tab 1'),
      ),
    );
  }
}
