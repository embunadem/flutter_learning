import 'package:flutter/material.dart';

class NavDrawer extends StatefulWidget {
  NavDrawer({Key? key}) : super(key: key);

  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Navigation Drawer'),
        ),
        body: Center(
          child: Text('Contoh Drawer'),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.brown[400],
                ),
                accountName: Text('Name'),
                accountEmail: Text('Name@email.com'),
                currentAccountPicture: CircleAvatar(
                  child: FlutterLogo(
                    size: 50,
                  ),
                  backgroundColor: Colors.white,
                ),
              ),
              ListTile(
                title: Text('Menu 1'),
                leading: Icon(Icons.home),
                onTap: () {},
              ),
              Divider(
                height: 2,
              ),
              ListTile(
                title: Text('Menu 2'),
                leading: Icon(Icons.alarm),
                onTap: () {},
              ),
              Divider(
                height: 2,
              ),
              ListTile(
                title: Text('Menu 3'),
                leading: Icon(Icons.people),
                trailing: Icon(Icons.chevron_right),
                onTap: () {},
              ),
              ListTile(
                title: Text('Menu 4'),
                trailing: Icon(Icons.calendar_today),
                onTap: () {},
              ),
              ListTile(
                title: Text('Menu 5'),
                trailing: Icon(Icons.calculate),
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
