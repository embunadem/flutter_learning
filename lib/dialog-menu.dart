import 'package:flutter/material.dart';

class DialogMenu extends StatelessWidget {
  const DialogMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Dialog menu'),
        ),
        body: ListView(
          padding: EdgeInsets.all(32),
          children: [
            RaisedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: Text('Logout'),
                    content: Text('Are you want to exit?'),
                    actions: [
                      FlatButton(
                        onPressed: () {},
                        child: Text('Yes'),
                      ),
                      FlatButton(
                        onPressed: () => Navigator.pop(context, 'No'),
                        child: Text('No'),
                      )
                    ],
                  ),
                );
              },
              color: Colors.red,
              child: Text('Alert Dialog'),
            ),
            RaisedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => SimpleDialog(
                    title: Text('Judul'),
                    children: [
                      ListTile(
                        leading: Icon(Icons.account_circle),
                        title: Text('email2@gmail.com'),
                        onTap: () => Navigator.pop(
                          context,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.account_circle),
                        title: Text('email3@gmail.com'),
                        onTap: () => Navigator.pop(
                          context,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.add_circle),
                        title: Text('Add account'),
                        onTap: () => Navigator.pop(
                          context,
                        ),
                      ),
                    ],
                  ),
                );
              },
              color: Colors.green,
              child: Text('Simple Dialog'),
            ),
            RaisedButton(
              onPressed: () {
                DateTime now = DateTime.now();
                showTimePicker(
                    context: context,
                    initialTime: TimeOfDay(
                      hour: now.hour,
                      minute: now.minute,
                    ));
              },
              color: Colors.blue,
              child: Text('Time Picker Dialog'),
            ),
            RaisedButton(
              onPressed: () {
                DateTime now = DateTime.now();
                showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2018),
                  lastDate: DateTime(2030),
                );
              },
              color: Colors.yellow,
              child: Text('Date Picker Dialog'),
            ),
          ],
        ),
      ),
    );
  }
}
