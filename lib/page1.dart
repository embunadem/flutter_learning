import 'package:belajar_flutter3/page2.dart';
import 'package:flutter/material.dart';

class Page1 extends StatefulWidget {
  Page1({Key? key}) : super(key: key);

  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Welcome Page 1'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Page2();
                  }));
                },
                color: Colors.blue,
                child: Text('Go to Page 2'),
              ),
              RaisedButton(
                onPressed: () {
                  //keluar page
                  Navigator.pop(context);
                },
                color: Colors.blue,
                child: Text('Back'),
              ),
            ],
          ),
        ));
  }
}
