import 'package:flutter/material.dart';

class PageviewMenu extends StatelessWidget {
  const PageviewMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Page View'),
        ),
        body: PageView(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: [
            Container(
              color: Colors.red,
              child: Center(
                child: Text('Page 1'),
              ),
            ),
            Container(
              color: Colors.green,
              child: Center(
                child: Text('Page 2'),
              ),
            ),
            Container(
              color: Colors.yellow,
              child: Center(
                child: Text('Page 3'),
              ),
            ),
            Container(
              color: Colors.blue,
              child: Center(
                child: Text('Page 4'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
