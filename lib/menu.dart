import 'package:flutter/material.dart';
import './menu-tab1.dart';
import './menu-tab2.dart';
import './menu-tab3.dart';

//==== TAB BAR ====
class Menu extends StatefulWidget {
  Menu({Key? key}) : super(key: key);

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  //buat bottom tab bar, sebenarnya dipakai idk but error
  // TabController _tabController;
  @override
  Widget build(BuildContext context) {
    //bukan return container
    return DefaultTabController(
      //length jangan lupa disesuaikan dengan jumlah tab
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green[800],
          title: Text('Whatsapp'),
          bottom: TabBar(
            isScrollable: false,
            indicatorColor: Colors.pink,
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.camera_alt),
              ),
              Tab(
                text: 'Chat',
              ),
              Tab(
                text: 'Call',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            MenuTab1(),
            MenuTab2(),
            MenuTab3(),
          ],
        ),
        // ===== BOTTOM TAP BAR =====
        bottomNavigationBar: Material(
          color: Colors.green[800],
          child: TabBar(
            // controller: _tabController,
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.camera),
                text: 'Story',
              ),
              Tab(
                icon: Icon(Icons.chat),
                text: 'Chat',
              ),
              Tab(
                icon: Icon(Icons.call),
                text: 'Call',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
