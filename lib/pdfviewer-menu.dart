import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

//syncfusion_flutter_pdfviewer 19.2.57-beta = pub.dev
class PdfviewerMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('PDF Viewer Menu'),
        ),
        body: SfPdfViewer.network(
            'https://www.tutorialspoint.com/cplusplus/cpp_tutorial.pdf'),
      ),
    );
  }
}
