import 'package:flutter/material.dart';

class MenuTab3 extends StatefulWidget {
  MenuTab3({Key? key}) : super(key: key);

  @override
  _MenuTab3State createState() => _MenuTab3State();
}

class _MenuTab3State extends State<MenuTab3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Page Call'),
      ),
      //float button
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.green,
        child: Icon(Icons.phone),
      ),
    );
  }
}
