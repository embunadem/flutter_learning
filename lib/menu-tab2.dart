import 'package:flutter/material.dart';

class MenuTab2 extends StatefulWidget {
  MenuTab2({Key? key}) : super(key: key);

  @override
  _MenuTab2State createState() => _MenuTab2State();
}

class _MenuTab2State extends State<MenuTab2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Page Chat'),
      ),
      //float button
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.green,
        child: Icon(Icons.message),
      ),
    );
  }
}
