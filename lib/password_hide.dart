import 'package:flutter/material.dart';

class PasswordHideMenu extends StatefulWidget {
  PasswordHideMenu({Key? key}) : super(key: key);

  @override
  _PasswordHideMenuState createState() => _PasswordHideMenuState();
}

class _PasswordHideMenuState extends State<PasswordHideMenu> {
  bool _securePass = true;
  showHide() {
    setState(() {
      _securePass = !_securePass;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Show Hide Password Menu'),
        ),
        body: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            children: [
              TextField(
                obscureText: _securePass,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.lock),
                  suffixIcon: IconButton(
                    icon: Icon(
                      _securePass ? Icons.visibility : Icons.visibility_off,
                    ),
                    onPressed: () {
                      showHide();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
