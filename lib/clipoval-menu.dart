import 'package:flutter/material.dart';

class ClipovalMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Clip Oval Menu'),
        ),
        body: ListView(
          children: [
            Center(
              child: ClipOval(
                child: Image.network(
                  'https://cdn.ebaumsworld.com/2020/01/13/021838/86172769/lego-star-wars-profile-70.jpg',
                  width: 200,
                  height: 200,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Divider(),
            Center(
              child: ClipOval(
                child: Image.network(
                  'https://www.ekor9.com/wp-content/uploads/2020/09/Gambar-zebra.jpg',
                  width: 200,
                  height: 200,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
