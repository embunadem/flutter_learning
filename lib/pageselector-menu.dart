import 'package:flutter/material.dart';

class PageselectorMenu extends StatelessWidget {
  static final kIcons = <Icon>[
    Icon(Icons.event),
    Icon(Icons.home),
    Icon(Icons.android),
    Icon(Icons.alarm),
    Icon(Icons.face),
    Icon(Icons.person),
    Icon(Icons.map),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Page Selector Menu'),
        ),
        body: DefaultTabController(
          length: kIcons.length,
          // length: 8,
          child: Builder(
            builder: (BuildContext context) => Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                children: [
                  TabPageSelector(),
                  Expanded(
                    child: IconTheme(
                      data: IconThemeData(
                        size: 128,
                        color: Theme.of(context).accentColor,
                      ),
                      child: TabBarView(
                        children: kIcons,
                        // children: [
                        //   Center(child: Text('Tab 1')),
                        //   Text('Tab 2'),
                        //   Text('Tab 3'),
                        //   Text('Tab 4'),
                        //   Text('Tab 5'),
                        //   Text('Tab 6'),
                        //   Text('Tab 7'),
                        //   Text('Tab 8'),
                        // ],
                      ),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      final TabController controller =
                          DefaultTabController.of(context);

                      if (!controller.indexIsChanging) {
                        controller.animateTo(kIcons.length - 1);
                      }
                    },
                    child: Text('Skip'),
                    color: Colors.yellow,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
