import 'package:flutter/material.dart';
import './menu_utama.dart';

//==== FLOATING BUTTON ====
class WidgetTest extends StatefulWidget {
  WidgetTest({Key? key}) : super(key: key);

  @override
  _WidgetTestState createState() => _WidgetTestState();
}

class _WidgetTestState extends State<WidgetTest> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Floating Button'),
          backgroundColor: Colors.green,
        ),
        body: Center(
          child: Text('Halaman'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          backgroundColor: Colors.green,
          child: Icon(Icons.message),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
          //biar shape mengikuti posisi floating button
          shape: CircularNotchedRectangle(),
          color: Colors.green,
          child: Row(
            children: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.menu),
              )
            ],
          ),
        ),
      ),
    );
  }
}
